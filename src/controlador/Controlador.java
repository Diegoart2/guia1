/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.Modelo;
import vista.Vista;

/**
 *
 * @author Casa
 */
public class Controlador implements ActionListener {

    private Vista view;
    private Modelo model;

    public Controlador(Vista view, Modelo model) {

        this.view = view;
        this.model = model;
        this.view.btnCalcular.addActionListener(this);

    }

    public void Iniciar() {
        view.setLocationRelativeTo(null);
        view.txtResultado.setEnabled(false);   
    }

    public void actionPerformed(ActionEvent e) {

        model.setGrados(Double.parseDouble(view.txtGrados.getText()));
        model.setHasta(Integer.parseInt(view.txtHasta.getText()));
        model.CalcularFormula();
        view.txtResultado.setText(String.valueOf(model.getResultado()));
    }
}
