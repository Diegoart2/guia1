/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Diego Rodriguez
 */
public class Modelo {

    private double Grados;
    private double Resultado;
    private int Hasta;

    public int getHasta() {
        return Hasta;
    }

    public void setHasta(int Hasta) {
        this.Hasta = Hasta;
    }

    public double getGrados() {
        return Grados;
    }

    public void setGrados(double Grados) {
        this.Grados = Grados;
    }

    public double getResultado() {
        return Resultado;
    }

    public void setResultado(double Resultado) {
        this.Resultado = Resultado;
    }

    public double CalcularFormula() {
        this.Resultado =0;
        for (int i = 0; i < this.Hasta + 1; i++) {
            /*Declaramos las variables*/
            int NumeroPositivo = 0;
            double SegundoCalculo = 0, Factorial=0, NumeroComun=0;
            
            /*Invoco los metodos pra cambiar de signo, calcular el factorial, y hacer una operacion*/
            NumeroComun=((2 * i) + 1);
            NumeroPositivo = CambioSigno(i);
            Factorial = CalFactorial(NumeroComun);
            SegundoCalculo = Math.pow(Math.toRadians(this.Grados), NumeroComun);
            
            
            this.Resultado = ((NumeroPositivo/Factorial)*SegundoCalculo)+this.Resultado;
        }
        return this.Resultado;
    }

    private int CambioSigno(int Numero) {
        int Resultado = 0;
        if (Numero % 2 == 0) {
            Resultado = 1;
        } else {
            Resultado = -1;
        }
        return Resultado;
    }

    private double CalFactorial(double Numero) {
        double factorial=1;
        
        for (double i = Numero; i > 0; i--) {
            factorial = factorial * i;
        }
        return factorial;

    }

}
