/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formulamatematica;

import controlador.Controlador;
import modelo.Modelo;
import vista.Vista;

/**
 *
 * @author Diego Rodriguez
 */
public class FormulaMatematica {

    public static void main(String[] args) {
        Modelo objModelo= new Modelo();
        Vista objVista= new Vista();
        Controlador objControlador = new Controlador(objVista,objModelo);
        
        objControlador.Iniciar();
        objVista.setVisible(true);
        
    }
    
}
